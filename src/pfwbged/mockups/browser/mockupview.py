from five import grok
from zope import interface, schema, component
from zope.interface import Interface
from z3c.form import form, field, button, widget
from zope.browserpage.viewpagetemplatefile import ViewPageTemplateFile

from collective.z3cform.chosen import ChosenMultiFieldWidget, ChosenFieldWidget
from plone.formwidget.querystring.widget import QueryStringFieldWidget

grok.templatedir('templates')


class IMockupForm(Interface):
    foobar = schema.TextLine(title=u'Test Ligne', required=False)
    date = schema.Date(title=u'Test Date', required=False)
    test_selection_simple = schema.Choice(title=u'Test choix simple',
            required=False,
            vocabulary=u'plone.app.vocabularies.AvailableContentLanguages')
    test_selection_multiple = schema.List(title=u'Test choix multiple',
            required=False,
            value_type=schema.Choice(vocabulary=u'plone.app.vocabularies.AvailableContentLanguages'))

    query = schema.List(
            title=u'Search terms',
            value_type=schema.Dict(value_type=schema.Field(),
                key_type=schema.TextLine()),
            required=False
            )



class MockupForm(form.Form):
    fields = field.Fields(IMockupForm)
    fields['test_selection_simple'].widgetFactory = ChosenFieldWidget
    fields['test_selection_multiple'].widgetFactory = ChosenMultiFieldWidget
    fields['query'].widgetFactory = QueryStringFieldWidget
    ignoreContext = True
    template = ViewPageTemplateFile('templates/view_form.pt')

    @button.buttonAndHandler(u'Button')
    def handleApply(self, action):
        pass


class MockupView(grok.View):
    grok.context(Interface)
    grok.name('mockup')
    template = grok.PageTemplateFile('templates/mockup_view.pt')

    def form(self):
        f = MockupForm(self.context, self.request)
        f.update()
        return f.render()
